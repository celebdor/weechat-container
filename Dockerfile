FROM docker.io/library/fedora:32

RUN dnf install weechat -y

ENTRYPOINT ["weechat", "--dir", "/weechat"]
